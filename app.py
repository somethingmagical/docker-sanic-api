#!/usr/bin/env python3
""" Proxies Docker requests.  Also can restart docker. """
import os
import requests
from proc.tree import logger, get_process_tree, ProcessNode
from proc.core import find_processes, Process
from sanic import Sanic
from sanic.response import json

app = Sanic()
DOCKER_REST_URL = 'http://172.17.0.1:2375'
ALLOWED_CONTAINERS = ['coder', 'cloudbot']
DISALLOWED_CONTAINERS = ['docker_proxy']


@app.route("/docker/restart")
async def docker_restart(request):
    """ Restarts the docker daemon. """
    init = get_process_tree()
    docker_proc = init.find(exe_name='dockerd')
    return json("{}")


@app.route("/docker/<container>/restart")
async def docker_container(request, container):
    """ Restarts a specific container. """
    if container in ALLOWED_CONTAINERS and container not in DISALLOWED_CONTAINERS:
      url = DOCKER_REST_URL + '/docker/' + container + '/restart'
      r = requests.post()
      return r.json


@app.route("/hello")
async def test(request):
    """ Test route for API. """
    return json({"hello": "world"})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
